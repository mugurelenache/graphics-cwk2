#pragma once
#include <string>
#include <GL/glu.h>
#include <vector>
#include "Material.h"
#include "Image.h"

class Mesh
{
	struct SubMesh 
	{
		std::vector<unsigned int> indices;
		// 0,1,2 pos -- 3,4,5 normal -- 6,7 uv -- total size 8
		std::vector<GLfloat> vertices;
		std::vector<GLfloat> normals;
		std::vector<GLfloat> uv;
		Material material = Materials::defaultM;		
	};

	public:
		std::vector<SubMesh> submeshes;
		Image* texture;

	private:
		bool loaded = 0;

	public:
		Mesh() = default;
		~Mesh();

		void draw();
		void loadObj(const std::string& path, Image* texture = nullptr); 
	
		static void calculateNormal(float* norm, float* coords1, float* coords2, float* coords3);
		static void fence(int len, float l, float w, float h);
		static void plank(float, float, float);
		static void pallet(int, float, float, float);
		static void teapot();
		static void polygon(int, int, int, int);
		static void wanted();
		static void terrain();
		static void cowboy();
		static void pyramid();
		static void cactus(int levels, int sublevelsA, int sublevelsB, int levelA, int levelB, bool ascendingA, bool ascendingB);
		static void sphere();
};
