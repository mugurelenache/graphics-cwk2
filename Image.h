#pragma once
#include <QImage>
#include <GL/glu.h>
#include <string>

class Image
{
	public:
		GLuint id;

	public:
		Image();
		Image(const std::string& path);
		~Image();

		void load();
		void unload();
};
