#pragma once
#include <Material.h>
#include <GL/glu.h>

class Bird
{
	public:
		Bird();
		~Bird();
		void setSpeed(float val);
		void setMaxAngle(int val);
		void setRadius(int val);
		void draw();
		void update();

	private:
		GLUquadric* body = nullptr;
		GLUquadric* tail = nullptr;
		GLUquadric* leftUpperWing = nullptr;
		GLUquadric* leftLowerWing = nullptr;
		GLUquadric* rightUpperWing = nullptr;
		GLUquadric* rightLowerWing = nullptr;
		GLUquadric* head = nullptr;
		GLUquadric* peak = nullptr;

		float speed = 1;
		float maxAngle = 25;
		float wingAngle = 0;
		float rotationAngle = 0;
		float flutter = 1;
		float radius = 5;
};
