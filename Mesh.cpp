#include "Mesh.h"
#include <GL/glu.h>
#include <GL/glut.h>
#include "Material.h"
#include "Image.h"
#include <stdio.h>
#include <fstream>
#include <iostream>
#include "OBJ_Loader.h" // CITE
#include <cmath>

Mesh::~Mesh()
{
}

void Mesh::loadObj(const std::string& path, Image* texture)
{
	objl::Loader loader;
	bool load = loader.LoadFile(path);

	if(load)
	{
		this->texture = texture;

		for(const auto subm : loader.LoadedMeshes)
		{
			SubMesh sm;

			sm.indices = subm.Indices; 

			for(const auto& i : subm.Vertices)
			{
				sm.vertices.push_back(i.Position.X);
				sm.vertices.push_back(i.Position.Y);
				sm.vertices.push_back(i.Position.Z);

				float len = i.Normal.X * i.Normal.X + i.Normal.Y * i.Normal.Y + i.Normal.Z * i.Normal.Z;

				sm.normals.push_back(i.Normal.X / len);
				sm.normals.push_back(i.Normal.Y / len);
				sm.normals.push_back(i.Normal.Z / len);

				sm.uv.push_back(i.TextureCoordinate.X);
				sm.uv.push_back(i.TextureCoordinate.Y);
			}

			if(subm.MeshMaterial.Ka.X)
			{
			sm.material.ambient[0] = subm.MeshMaterial.Ka.X;
			sm.material.ambient[1] = subm.MeshMaterial.Ka.Y;
			sm.material.ambient[2] = subm.MeshMaterial.Ka.Z;
			sm.material.ambient[3] = 1;
			}

			if(subm.MeshMaterial.Kd.X)
			{
			sm.material.diffuse[0] = subm.MeshMaterial.Kd.X;
			sm.material.diffuse[1] = subm.MeshMaterial.Kd.Y;
			sm.material.diffuse[2] = subm.MeshMaterial.Kd.Z;
			sm.material.diffuse[3] = 1;
			}

			if(subm.MeshMaterial.Ks.X)
			{
			sm.material.specular[0] = subm.MeshMaterial.Ks.X;
			sm.material.specular[1] = subm.MeshMaterial.Ks.Y;
			sm.material.specular[2] = subm.MeshMaterial.Ks.Z;
			sm.material.specular[3] = 1;
			}

			if(subm.MeshMaterial.Ns)
			{
			sm.material.shininess = subm.MeshMaterial.Ns;
			}
			submeshes.push_back(sm);
		}
	}
}



void Mesh::draw()
{
	for(auto& sm : submeshes)
	{
		glColor3f(1,1,1);
		sm.material.set();
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_INDEX_ARRAY);
		glEnableClientState(GL_NORMAL_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);


		glMaterialf(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, 1);
		if(texture)
			texture->load();

		glVertexPointer(3, GL_FLOAT, 0, sm.vertices.data());
		glNormalPointer(GL_FLOAT, 0, sm.normals.data()); 
		glTexCoordPointer(2, GL_FLOAT, 0, sm.uv.data()); 

		glDrawElements(GL_TRIANGLES, sm.indices.size(), GL_UNSIGNED_INT, sm.indices.data());

		if(texture)
			texture->unload();

		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_NORMAL_ARRAY);
		glDisableClientState(GL_INDEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);

		sm.material.unset();
	}
}

void Mesh::cactus(int levels, int sublevelsA, int sublevelsB, int levelA, int levelB, bool ascendingA, bool ascendingB)
{
	float w = 1;
	float l = 1;
	float h = 1;

	// Add material

	Materials::cactus.set();
	for(int i = 0; i < levels; i++)
	{
		glPushMatrix();
			glTranslatef(0, 2 * h * i, 0);
			Mesh::plank(l, w, h);

			if(i == levelA && levelA != 0)
			{
				glPushMatrix();
				
				for(int j = 1; j < 4; j++)
				{
					glTranslatef(2 * l, 0, 0);
					Mesh::plank(l, w, h);
				}

				for(int k = 1; k <= sublevelsB; k++)
				{
					if(ascendingA)
					{
						glTranslatef(0, 2 * h, 0);
					}
					else
					{
						glTranslatef(0, -2 * h, 0);
					}
					Mesh::plank(l, w, h);
				}

				glPopMatrix();
			}
			else if(i == levelB && levelB != 0)
			{
				glPushMatrix();
				
				for(int j = 1; j < 4; j++)
				{
					glTranslatef(-2 * l, 0, 0);
					Mesh::plank(l, w, h);
				}

				for(int k = 1; k <= sublevelsA; k++)
				{
					if(ascendingB)
					{ 
						glTranslatef(0, 2 * h, 0);
					}
					else
					{
						glTranslatef(0, -2 * h, 0);
					}

					Mesh::plank(l, w, h);
				}

				glPopMatrix();
			}

		glPopMatrix();
	}

	Materials::cactus.unset();

}

void Mesh::pyramid()
{
	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 0.0f, 0.0f); 

	//	Front
	glVertex3f( 0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	// Right
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	// Back
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glColor3f(1.0f, 1.0f, 1.0f); 

	// Left
	glVertex3f( 0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f,-1.0f,-1.0f);
	glVertex3f(-1.0f,-1.0f, 1.0f);
	glEnd();  
}

void Mesh::sphere()
{
	int NR_PHI = 20;
	int NR_THETA = 20;

	for(int longitude = 0; longitude < NR_PHI; longitude++)
	{
		for(int latitude = 0; latitude < NR_THETA; latitude++)
		{
			float d_phi   = 2*M_PI/NR_PHI;
			float d_theta = M_PI/NR_THETA; 
			glBegin(GL_TRIANGLES);
			double x, y, z;

			x = cos(longitude*d_phi)*sin(latitude*d_theta);
			y = sin(longitude*d_phi)*sin(latitude*d_theta);
			z = cos(latitude*d_theta);
			glNormal3f(x, y, z);
			glTexCoord2f(static_cast<float>(longitude)/NR_PHI, static_cast<float>(latitude)/NR_THETA);
			glVertex3f(x, y, z);
			x = cos((longitude+1)*d_phi)*sin(latitude*d_theta);
			y = sin((longitude+1)*d_phi)*sin(latitude*d_theta);
			z = cos(latitude*d_theta);
			glNormal3f(x, y, z);
			glTexCoord2f(d_phi,0);
			glTexCoord2f(static_cast<float>(longitude+1)/NR_PHI, static_cast<float>(latitude)/NR_THETA);
			glVertex3f(x, y, z);
			x = cos((longitude+1)*d_phi)*sin((latitude+1)*d_theta);
			y = sin((longitude+1)*d_phi)*sin((latitude+1)*d_theta);
			z = cos((latitude+1)*d_theta);
			glNormal3f(x, y, z);
			glTexCoord2f(static_cast<float>(longitude+1)/NR_PHI, static_cast<float>(latitude+1)/NR_THETA);
			glVertex3f(x, y, z);      

			x = cos(longitude*d_phi)*sin(latitude*d_theta);
			y = sin(longitude*d_phi)*sin(latitude*d_theta);
			z = cos(latitude*d_theta); 
			glNormal3f(x, y, z);
			glTexCoord2f(static_cast<float>(longitude)/NR_PHI, static_cast<float>(latitude)/NR_THETA);
			glVertex3f(x, y, z);
			x = cos((longitude+1)*d_phi)*sin((latitude+1)*d_theta);
			y = sin((longitude+1)*d_phi)*sin((latitude+1)*d_theta);
			z = cos((latitude+1)*d_theta);
			glNormal3f(x, y, z);
			glTexCoord2f(static_cast<float>(longitude+1)/NR_PHI, static_cast<float>(latitude+1)/NR_THETA);
			glVertex3f(x, y, z);
			x = cos((longitude)*d_phi)*sin((latitude+1)*d_theta);
			y = sin((longitude)*d_phi)*sin((latitude+1)*d_theta);
			z = cos((latitude+1)*d_theta);
			glNormal3f(x, y, z);
			glTexCoord2f(static_cast<float>(longitude)/NR_PHI, static_cast<float>(latitude+1)/NR_THETA);
			glVertex3f(x, y, z);      

			glEnd();
		}
	}	
}

void Mesh::wanted()
{
	GLfloat normals[][3] = { {0, 1., 0} };	
	// Materials::whiteShiny.set();

	glNormal3fv(normals[0]);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(-1.0, 0,  1.0);
	glTexCoord2f(1.0, 0.0);
	glVertex3f( 1.0, 0,  1.0);
	glTexCoord2f(1.0, 1.0);
	glVertex3f( 1.0, 0, -1.0);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(-1.0, 0, -1.0);
	glEnd();
}

void Mesh::terrain()
{
	GLfloat normals[][3] = { {0, 1., 0} };	
	Materials::whiteShiny.set();

	// Load texture TODO
	glNormal3fv(normals[0]);
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(-1.0, 0,  1.0);
	glTexCoord2f(30.0, 0.0);
	glVertex3f( 1.0, 0,  1.0);
	glTexCoord2f(30.0, 30.0);
	glVertex3f( 1.0, 0, -1.0);
	glTexCoord2f(0.0, 30.0);
	glVertex3f(-1.0, 0, -1.0);
	glEnd();
}

void Mesh::plank(float len, float width, float depth)
{
	GLfloat normals[][3] = { {1., 0. ,0.}, {-1., 0., 0.}, {0., 0., 1.}, {0., 0., -1.}, {0., 1., 0.}, {0., -1., 0.} };

	// Right face
	glNormal3fv(normals[0]);
	glBegin(GL_POLYGON);
	glVertex3f( len, -depth,  width);
	glVertex3f( len, -depth, -width);
	glVertex3f( len,  depth, -width);
	glVertex3f( len,  depth,  width);
	glEnd();


	// front face
	glNormal3fv(normals[3]); 
	glBegin(GL_POLYGON);
	glVertex3f(-len, -depth, -width);
	glVertex3f( len, -depth, -width);
	glVertex3f( len,  depth, -width);
	glVertex3f(-len,  depth, -width);
	glEnd();


	// back face
	glNormal3fv(normals[2]); 
	glBegin(GL_POLYGON);
	glVertex3f(-len, -depth, width);
	glVertex3f( len, -depth, width);
	glVertex3f( len,  depth, width);
	glVertex3f(-len,  depth, width);
	glEnd();


	//left face
	glNormal3fv(normals[1]);
	glBegin(GL_POLYGON);
	glVertex3f(-len, -depth,  width);
	glVertex3f(-len, -depth, -width);
	glVertex3f(-len,  depth, -width);
	glVertex3f(-len,  depth,  width);
	glEnd();

	//top face
	glNormal3fv(normals[4]);
	glBegin(GL_POLYGON);
	glVertex3f(-len,  depth,  width);
	glVertex3f( len,  depth,  width);
	glVertex3f( len,  depth, -width);
	glVertex3f(-len,  depth, -width);
	glEnd();

	//bottom face
	glNormal3fv(normals[5]);
	glBegin(GL_POLYGON);
	glVertex3f(-len, -depth,  width);
	glVertex3f( len, -depth,  width);
	glVertex3f( len, -depth, -width);
	glVertex3f(-len, -depth, -width);
	glEnd();
}

void Mesh::pallet(int len, float plankLen, float plankWidth, float plankDepth)
{
	glPushMatrix();
	// glTranslatef(0, 0, -plankWidth * ((float)len) + (plankWidth));

	for(int i = 0; i < len; i++)
	{
		if(i % 2)
		{
			Materials::plank.set();
		}
		else
		{
			Materials::plankV1.set();
		}

		glPushMatrix();
		glTranslatef(0, 0, i * (plankWidth * 2)); 
		Mesh::plank(plankLen, plankWidth, plankDepth);	
		glPopMatrix();

	}

	Materials::defaultM.set();
	glPopMatrix();
}

void Mesh::fence(int len, float l, float w, float h)
{
	Materials::plank.set();

	for(int i = 0; i < len; i++)
	{
		glPushMatrix();

		glTranslatef(2 * (l * 2) * i, 0, 0);
		Mesh::plank(l, w, h);

		if(i < len - 1)
		{
			glPushMatrix();
			glTranslatef(2 * l, h/2, 0.);
			Mesh::plank(l, w/2, 0.1);
			glPopMatrix();

			glPushMatrix();
			glTranslatef(2 * l, -h/2, 0);
			Mesh::plank(l, w/2, 0.1);
			glPopMatrix();
		}
		glPopMatrix();
	}
}


void teapot()
{
	Material* teapotMaterial = &Materials::brass;
	teapotMaterial->set();
	glutSolidTeapot(1.0);
}


void Mesh::cowboy()
{
	// Body
	glPushMatrix();
	// TODO move body and stuff

	{
		glPushMatrix();
		GLUquadric* body = gluNewQuadric();
		Materials::brass.set();
		glTranslatef(0, 0, -2.5); // center it
		glScalef(1, 1, 1);
		gluCylinder(body, 1, 1.2, 5, 20, 5);
		glPopMatrix();
	}
	// Pistol
	{
	}

	// Neck
	{
		glPushMatrix();

		GLUquadric* neck = gluNewQuadric();
		glTranslatef(0, 0, 2.5);
		glScalef(0.3, 0.3, 0.5);
		gluCylinder(neck, 1, 1, 1, 6, 3);

		// Head TODO move out
		{
			glPushMatrix();

			GLUquadric* head = gluNewQuadric();
			glTranslatef(0, 0, 2.5);
			gluSphere(head, 2, 5, 5);

			glPopMatrix();
		}

		glPopMatrix();		
	}




	// Left upper leg
	{
		glPushMatrix();
		GLUquadric* leftUpperLeg = gluNewQuadric();
		glTranslatef(-0.5, 0, -5);
		gluCylinder(leftUpperLeg, 0.3, 0.3, 3, 6, 6);
		{
			// Left Knee
			glPushMatrix();
			GLUquadric* leftKnee = gluNewQuadric();
			gluSphere(leftKnee, 0.3, 6, 6);

			// Left lower leg
			{
				glPushMatrix();
				GLUquadric* leftLowerLeg = gluNewQuadric();
				glTranslatef(-0, 0, -2);
				gluCylinder(leftLowerLeg, 0.25, 0.25, 2, 6, 6);

				// Left shoe
				{
					glPushMatrix();
					glPopMatrix();
				}
				glPopMatrix();
			}
			glPopMatrix();
		}
		glPopMatrix();
	}

	// Right upper leg
	{
		glPushMatrix();
		GLUquadric* rightUpperLeg = gluNewQuadric();
		glTranslatef(0.5, 0, -5);
		gluCylinder(rightUpperLeg, 0.3, 0.3, 3, 6, 6);
		{
			// Right Knee
			glPushMatrix();
			GLUquadric* rightKnee = gluNewQuadric();
			gluSphere(rightKnee, 0.3, 6, 6);

			// Right lower leg
			{
				glPushMatrix();
				GLUquadric* rightLowerLeg = gluNewQuadric();
				glTranslatef(-0, 0, -2);
				gluCylinder(rightLowerLeg, 0.25, 0.25, 2, 6, 6);

				// Right shoe
				{
					glPushMatrix();
					// TODO add shoes
					glPopMatrix();
				}
				glPopMatrix();
			}
			glPopMatrix();
		}
		glPopMatrix();
	}

	// Left shoulder
	{
		glPushMatrix();
		GLUquadric* leftShoulder = gluNewQuadric();
		glTranslatef(-1.4, 0, 2.2);
		gluSphere(leftShoulder, 0.3, 6, 6);

		// Upper arm
		{
			glPushMatrix();
			GLUquadric* leftUpperArm = gluNewQuadric();
			glRotatef(-180 + 30, 0, 1, 0);
			gluCylinder(leftUpperArm, 0.3, 0.3, 2, 6, 6);

			// Left Elbow
			{
				glPushMatrix();
				GLUquadric* leftElbow = gluNewQuadric();
				glTranslatef(-0, 0, 2 + 0.35 / 2.0);
				gluSphere(leftElbow, 0.35, 6, 6);

				// Lower arm 
				{
					glPushMatrix();
					GLUquadric* leftLowerArm = gluNewQuadric();
					gluCylinder(leftLowerArm, 0.25, 0.25, 2, 6, 6);

					// Left hand
					{
						glPushMatrix();
						GLUquadric* leftHand = gluNewQuadric();
						glTranslatef(0, 0, 2.2);
						gluSphere(leftHand, 0.4, 6, 6);
						glPopMatrix();
					}
					glPopMatrix();
				}
				glPopMatrix();
			}
			glPopMatrix();
		}
		glPopMatrix();
	}

	// Right shoulder
	{
		glPushMatrix();
		GLUquadric* rightShoulder = gluNewQuadric();
		glTranslatef(1.4, 0, 2.2);
		gluSphere(rightShoulder, 0.3, 6, 6);

		// Upper arm
		{
			glPushMatrix();
			GLUquadric* rightUpperArm = gluNewQuadric();
			glRotatef(-180 - 30, 0, 1, 0);
			gluCylinder(rightUpperArm, 0.3, 0.3, 2, 6, 6);

			// Right Elbow
			{
				glPushMatrix();
				GLUquadric* rightElbow = gluNewQuadric();
				glTranslatef(-0, 0, 2 + 0.35 / 2.0);
				gluSphere(rightElbow, 0.35, 6, 6);

				// Lower arm 
				{
					glPushMatrix();
					GLUquadric* rightLowerArm = gluNewQuadric();
					gluCylinder(rightLowerArm, 0.25, 0.25, 2, 6, 6);

					// Right hand
					{
						glPushMatrix();
						GLUquadric* rightHand = gluNewQuadric();
						glTranslatef(0, 0, 2.2);
						gluSphere(rightHand, 0.4, 6, 6);
						glPopMatrix();
					}
					glPopMatrix();
				}
				glPopMatrix();
			}
			glPopMatrix();
		}
		glPopMatrix();
	}

	glPopMatrix();
}

void Mesh::calculateNormal(float* norm, float* coords1, float* coords2, float* coords3)
{
	float aCoords[3];
	float bCoords[3];
	float rCoords[3];
	float len = 0;

	aCoords[0] = coords1[0] - coords2[0];
	aCoords[1] = coords1[1] - coords2[1];
	aCoords[2] = coords1[2] - coords2[2];

	bCoords[0] = coords1[0] - coords3[0];
	bCoords[1] = coords1[1] - coords3[1];
	bCoords[2] = coords1[2] - coords3[2];

	rCoords[0] = aCoords[1] * bCoords[2] - bCoords[1] * aCoords[2];
	rCoords[1] = aCoords[2] * bCoords[0] - bCoords[2] * aCoords[0];
	rCoords[2] = aCoords[0] * bCoords[1] - bCoords[0] * aCoords[1];

	len = std::sqrt(rCoords[0] * rCoords[0] + rCoords[1] * rCoords[1] + rCoords[2] * rCoords[2]);

	norm[0] = rCoords[0] / len;
	norm[1] = rCoords[1] / len;
	norm[2] = rCoords[2] / len;
}
