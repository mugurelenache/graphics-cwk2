#pragma once
#include <QGLWidget>
#include <QMenuBar>
#include <QSlider>
#include <QBoxLayout>
#include <QGroupBox>
#include <QTimer>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QRadioButton>
#include <QPushButton>
#include "RendererWidget.h"

class RendererWindow: public QWidget
{ 
	public:
		RendererWindow(QWidget *parent);
		~RendererWindow();

		QHBoxLayout* principalLayout;
	
		QGroupBox* openglGroupBox;
		QGroupBox* settingsGroupBox;

		QVBoxLayout* openglLayout;
		QVBoxLayout* settingsLayout;

		// beneath that, the main widget
		RendererWidget *rendererWidget;

		// and a slider for the number of vertices
		QTimer* timer;

		QLabel* movementLabel;

		QSlider* birdSpeedSlider;
		QSlider* birdMaxAngleSlider;
		QSlider* birdCircleRadiusSlider;
		QSlider* windSpeedSlider;
		QSlider* dustCountSlider;
		QSlider* wantedLevelSlider;
		

		QGroupBox* tumbleGroup;
		QRadioButton* tumbleOff;
		QRadioButton* tumbleOn;
		QVBoxLayout* tumbleLayout;


		QLabel* windSpeedLabel;
		QLabel* dustCountLabel;
		QLabel* wantedLevelLabel;
		QLabel* birdSpeedLabel;
		QLabel* birdMaxAngleLabel;
		QLabel* birdCircleRadiusLabel;

		QPushButton* shootButton;

		// resets all the interface elements
		void ResetInterface();
}; 

