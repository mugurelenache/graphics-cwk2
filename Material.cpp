#include "Material.h"

void Material::set()
{
	glMaterialfv(GL_FRONT, GL_AMBIENT, this->ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, this->diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, this->specular);
	glMaterialf(GL_FRONT, GL_SHININESS, this->shininess);
}

void Material::unset()
{
	Materials::defaultM.set();
}

namespace Materials 
{
	Material brass = {
		{ 0.33, 0.22, 0.03, 1.0 },
		{ 0.78, 0.57, 0.11, 1.0 },
		{ 0.99, 0.91, 0.81, 1.0 },
		27.8
	};	

	Material plank = {
		{ 0.33, 0.22, 0.03, 1.0 },
		{ 0.76, 0.61, 0.32, 1.0 },
		{ 0.99, 0.91, 0.81, 1.0 },
		27.8
	};	
	
	Material cactus = {
		{ 0.34, 0.42, 0.11, 1.0 },
		{ 0.34, 0.42, 0.11, 1.0 },
		{ 0.36, 0.31, 0.32, 1.0 },
		17.8
	};	
	
	Material plankV1 = {
		{ 0.33, 0.22, 0.03, 1.0 },
		{ 0.47, 0.38, 0.15, 1.0 },
		{ 0.99, 0.91, 0.81, 1.0 },
		27.8
	};	

	Material whiteShiny = {
		{ 1.0, 1.0, 1.0, 1.0 },
		{ 1.0, 1.0, 1.0, 1.0 },
		{ 1.0, 1.0, 1.0, 1.0 },
		100.0
	};

	Material defaultM = {
		{ 1.0, 1.0, 1.0, 1.0 },
		{ 1.0, 1.0, 1.0, 1.0 },
		{ 1.0, 1.0, 1.0, 1.0 },
		10.0
	};
}
