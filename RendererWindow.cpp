#include "RendererWindow.h"
#include <QUrl>

// constructor / destructor
RendererWindow::RendererWindow(QWidget *parent) : QWidget(parent)
{ 

	principalLayout = new QHBoxLayout(this);

	openglGroupBox = new QGroupBox();
	settingsGroupBox = new QGroupBox();
	settingsGroupBox->setMaximumWidth(300);

	principalLayout->addWidget(openglGroupBox);
	principalLayout->addWidget(settingsGroupBox);

	setLayout(principalLayout);

	openglLayout = new QVBoxLayout();
	settingsLayout = new QVBoxLayout();
	settingsLayout->setAlignment(Qt::AlignTop);

	openglGroupBox->setLayout(openglLayout);
	settingsGroupBox->setLayout(settingsLayout);



	rendererWidget = new RendererWidget(this);
	openglLayout->addWidget(rendererWidget);




	birdSpeedLabel = new QLabel();
	birdSpeedLabel->setText("Bird Speed:");
	settingsLayout->addWidget(birdSpeedLabel);


	birdSpeedSlider = new QSlider(Qt::Horizontal);
	connect(birdSpeedSlider, SIGNAL(valueChanged(int)), rendererWidget, SLOT(updateBirdSpeed(int)));
	settingsLayout->addWidget(birdSpeedSlider);	
	birdSpeedSlider->setValue(100);
	birdSpeedSlider->setMinimum(1);
	birdSpeedSlider->setMaximum(280);





	birdMaxAngleLabel = new QLabel();
	birdMaxAngleLabel->setText("Bird's Wings Flutter Angle:");
	settingsLayout->addWidget(birdMaxAngleLabel);


	birdMaxAngleSlider = new QSlider(Qt::Horizontal);
	connect(birdMaxAngleSlider, SIGNAL(valueChanged(int)), rendererWidget, SLOT(updateBirdWingAngle(int)));
	settingsLayout->addWidget(birdMaxAngleSlider);
	birdMaxAngleSlider->setValue(25);
	birdMaxAngleSlider->setMinimum(5);
	birdMaxAngleSlider->setMaximum(30);





	birdCircleRadiusLabel = new QLabel();
	birdCircleRadiusLabel->setText("Bird's Flight Circle Radius:");
	settingsLayout->addWidget(birdCircleRadiusLabel);


	birdCircleRadiusSlider = new QSlider(Qt::Horizontal);
	connect(birdCircleRadiusSlider, SIGNAL(valueChanged(int)), rendererWidget, SLOT(updateBirdCircleRadius(int)));
	settingsLayout->addWidget(birdCircleRadiusSlider);	
	birdCircleRadiusSlider->setValue(5);
	birdCircleRadiusSlider->setMinimum(2);
	birdCircleRadiusSlider->setMaximum(40);




	windSpeedLabel = new QLabel();
	windSpeedLabel->setText("Wind Speed:");
	settingsLayout->addWidget(windSpeedLabel);

	windSpeedSlider = new QSlider(Qt::Horizontal);
	connect(windSpeedSlider, SIGNAL(valueChanged(int)), rendererWidget, SLOT(updateWindSpeed(int)));	windSpeedSlider->setValue(2);
	windSpeedSlider->setMinimum(0);
	windSpeedSlider->setMaximum(5);
	settingsLayout->addWidget(windSpeedSlider);

	dustCountLabel = new QLabel();
	dustCountLabel->setText("Dust Particles Count:");
	settingsLayout->addWidget(dustCountLabel);

	dustCountSlider = new QSlider(Qt::Horizontal);
	connect(dustCountSlider, SIGNAL(valueChanged(int)), rendererWidget, SLOT(updateDustCount(int)));
	dustCountSlider->setValue(5000);
	dustCountSlider->setMinimum(0);
	dustCountSlider->setMaximum(5000);
	settingsLayout->addWidget(dustCountSlider);

	wantedLevelLabel = new QLabel();
	wantedLevelLabel->setText("Madman Wanted Level:");
	settingsLayout->addWidget(wantedLevelLabel);

	wantedLevelSlider = new QSlider(Qt::Horizontal);
	connect(wantedLevelSlider, SIGNAL(valueChanged(int)), rendererWidget, SLOT(updateWantedLevel(int)));
	wantedLevelSlider->setValue(50);
	wantedLevelSlider->setMinimum(0);
	wantedLevelSlider->setMaximum(5000);
	settingsLayout->addWidget(wantedLevelSlider);

	tumbleGroup = new QGroupBox(tr("Tumbleweed Togglers:"));
	tumbleLayout = new QVBoxLayout();
	tumbleOn = new QRadioButton(tr("Tumbleweed - ON"));
	tumbleOn->setChecked(true);
	tumbleOff = new QRadioButton(tr("Tumbleweed - OFF"));
	tumbleLayout->addWidget(tumbleOn);
	tumbleLayout->addWidget(tumbleOff);
	tumbleGroup->setLayout(tumbleLayout);

	connect(tumbleOn, SIGNAL(clicked()), rendererWidget, SLOT(showTumbleweed()));
	connect(tumbleOff, SIGNAL(clicked()), rendererWidget, SLOT(hideTumbleweed()));

	settingsLayout->addWidget(tumbleGroup);

	movementLabel = new QLabel();
	movementLabel->setText("Camera controls: WASD EQ");
	settingsLayout->addWidget(movementLabel);


	timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), rendererWidget, SLOT(updateBirdAnim()));
	connect(timer, SIGNAL(timeout()), rendererWidget, SLOT(updateCowboysMotion()));
	timer->start(0);

	shootButton = new QPushButton();
	shootButton->setText("Shoot!");
	connect(shootButton, SIGNAL(pressed()), rendererWidget, SLOT(shoot()));
	settingsLayout->addWidget(shootButton);
}


RendererWindow::~RendererWindow()
{
	delete rendererWidget;
	delete windSpeedSlider;
	delete dustCountSlider;
	delete wantedLevelSlider;
	delete birdSpeedSlider;
	delete birdMaxAngleSlider;
	delete timer;
	delete openglLayout;
	delete settingsLayout;
	delete openglGroupBox;
	delete settingsGroupBox;
	delete principalLayout;
} 

// resets all the interface elements
void RendererWindow::ResetInterface()
{
	birdMaxAngleSlider->setMinimum(5);
	birdMaxAngleSlider->setMaximum(30);

	birdCircleRadiusSlider->setMinimum(2);
	birdCircleRadiusSlider->setMaximum(20);

	windSpeedSlider->setMinimum(0);
	windSpeedSlider->setMaximum(5);

	dustCountSlider->setMinimum(0);
	windSpeedSlider->setMaximum(5000);

	windSpeedSlider->setMinimum(0);
	windSpeedSlider->setMaximum(5000);


	rendererWidget->update();
	update();
} 
