#include "Image.h"
#include <QString>
#include <QGLWidget>


#include <iostream>
Image::Image()
{
}

Image::Image(const std::string& path)
{
	QImage image, data;
   
	if(!image.load(QString::fromStdString(path)))
	{
		std::cerr << "Could not load file." << std::endl;
		std::exit(-1);
	}

	data = QGLWidget::convertToGLFormat(image);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glGenTextures(1, &id);
	glBindTexture(GL_TEXTURE_2D, id);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, data.width(), data.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, data.bits());

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, data.width(), data.height(), GL_RGBA, GL_UNSIGNED_BYTE, data.bits());

	glBindTexture(GL_TEXTURE_2D, 0);
}


void Image::load()
{
	glBindTexture(GL_TEXTURE_2D, id);	
}

void Image::unload()
{
	glBindTexture(GL_TEXTURE_2D, 0);
}

Image::~Image()
{
	glDeleteTextures(1, &id);	
}
