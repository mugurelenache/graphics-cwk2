#pragma once
#include "Particle.h"
#include <vector>

class Image;
class ParticleGenerator
{
	public:
		ParticleGenerator(Image* texture);
		~ParticleGenerator();
	
		void generate(int n, float x, float y, float z, float sx, float sy, float sz, bool random, bool isBlood);
		void drawParticles();

	public:
		std::vector<Particle*> particles;
		Image* texture;
		bool spawned = false;
};
