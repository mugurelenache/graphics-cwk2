#include "Camera.h"
#include <cmath>

Camera::Camera()
{
}

Camera::Camera(GLfloat x, GLfloat y, GLfloat z, GLfloat centerX, GLfloat centerY, GLfloat centerZ, GLfloat nearPane, GLfloat farPlane, GLfloat width, GLfloat height)
{
	this->position[0] = x;
	this->position[1] = y;
	this->position[2] = z;

	this->lookingAt[0] = centerX;
	this->lookingAt[1] = centerY;
	this->lookingAt[2] = centerZ;

	this->nearPlane = nearPane;
	this->farPlane = farPlane;
	this->width = width;
	this->height = height;
}

Camera::Camera(GLfloat position[3], GLfloat center[3], GLfloat nearPane, GLfloat farPlane, GLfloat width, GLfloat height)
{
	this->position[0] = position[0];
	this->position[1] = position[1];
	this->position[2] = position[2];

	this->lookingAt[0] = center[0];
	this->lookingAt[1] = center[1];
	this->lookingAt[2] = center[2];
	
	this->nearPlane = nearPane;
	this->farPlane = farPlane;
	this->width = width;
	this->height = height;
}

Camera::~Camera()
{
}

void Camera::setPosition(GLfloat x, GLfloat y, GLfloat z)
{
	this->position[0] = x;
	this->position[1] = y;
	this->position[2] = z;
}

void Camera::setPosition(GLfloat position[3])
{
	this->position[0] = position[0];
	this->position[1] = position[1];
	this->position[2] = position[2];
}

void Camera::setOrientation(GLfloat x, GLfloat y, GLfloat z)
{
	this->lookingAt[0] = x;
	this->lookingAt[1] = y;
	this->lookingAt[2] = z;
}

void Camera::setOrientation(GLfloat position[3])
{
	this->lookingAt[0] = position[0];
	this->lookingAt[1] = position[1];
	this->lookingAt[2] = position[2];
}

void Camera::translateOrientation(GLfloat x, GLfloat y, GLfloat z)
{
	this->lookingAt[0] += x;
	this->lookingAt[1] += y;
	this->lookingAt[2] += z;

	this->yaw += x;
	this->pitch += y;
}

void Camera::translateOrientation(GLfloat position[3])
{
	this->lookingAt[0] += position[0];
	this->lookingAt[1] += position[1];
	this->lookingAt[2] += position[2];
}

void Camera::translate(GLfloat x, GLfloat y, GLfloat z)
{
	if(this->position[0] + x < -25 || this->position[0] + x > 25)
		x = 0;
	if(this->position[1] + y < 0 || this->position[1] + y > 50)
		y = 0;   
	if(this->position[2] + z < 5 || this->position[2] + z > 100)
		z = 0;
	this->position[0] += x;
	this->position[1] += y;
	this->position[2] += z;
}

void Camera::translate(GLfloat position[3])
{
	this->lookingAt[0] += position[0];
	this->lookingAt[1] += position[1];
	this->lookingAt[2] += position[2];
}

void Camera::load()
{
	glLoadIdentity();
	gluLookAt(position[0], position[1], position[2], lookingAt[0], lookingAt[1], lookingAt[2], up[0], up[1], up[2]);
}

void Camera::resize(int newWidth, int newHeight)
{
	this->width = newWidth;
	this->height = newHeight;
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45, width / height, nearPlane, farPlane);
}
