#include "Bullet.h"
#include "Mesh.h"
#include <iostream>

Bullet::Bullet(float x, float y, float z)
{
	direction[0] = x;
	direction[1] = y;
	direction[2] = z;
}

Bullet::~Bullet()
{
}

void Bullet::draw()
{
	Mesh::plank(0.3, 0.1, 0.1);
	// Add line trail
}

void Bullet::updateBulletMotion()
{
	keyframes += speed;
	glPushMatrix();
	{
		glTranslatef(direction[0] * keyframes, direction[1] * keyframes, direction[2] * keyframes);
		glScalef(30, 30, 30);
		this->draw();
	}
	glPopMatrix();
}
