#pragma once
#include <GL/glu.h>

class Camera
{
	public:
		GLfloat position[3] = { 0 };
		GLfloat lookingAt[3] = { 0, 0, -1.0 };

		GLfloat forward[3] = {0, 0, -1};
		GLfloat right[3] = {1, 0, 0};
		GLfloat up[3] = {0, 1, 0};


		GLfloat nearPlane = 0.01f;
		GLfloat farPlane = 100.0f;
		GLfloat width = 1024;
		GLfloat height = 768 + 112;

		GLfloat yaw = -90.f; 
		GLfloat pitch = 0;

	public:
		Camera();
		Camera(GLfloat x, GLfloat y, GLfloat z, GLfloat centerX, GLfloat centerY, GLfloat centerZ, GLfloat nearPlane, GLfloat farPlane, GLfloat width, GLfloat height);
		Camera(GLfloat position[3], GLfloat center[3], GLfloat nearPlane, GLfloat farPlane, GLfloat width, GLfloat height);
		~Camera();

		void setPosition(GLfloat x, GLfloat y, GLfloat z);
		void setPosition(GLfloat position[3]);

		void setOrientation(GLfloat x, GLfloat y, GLfloat z);
		void setOrientation(GLfloat position[3]);

		void translate(GLfloat x, GLfloat y, GLfloat z);
		void translate(GLfloat position[3]);

		void translateFB(bool fwd);
		void translateLR(bool left);

		void translateOrientation(GLfloat x, GLfloat y, GLfloat z);
		void translateOrientation(GLfloat position[3]);

		void load();
		void resize(int newWidth, int newHeight);
};

