#pragma once
#include "Material.h"
#include "Mesh.h"
#include "Bullet.h"
#include <vector>
#include <GL/glu.h>

class ParticleGenerator;
class Cowboy 
{
	public:
		Cowboy(Image* texture, Mesh* leftBoot, Mesh* rightBoot, Mesh* hat, Mesh* pistol, ParticleGenerator* particleGenerator);
		~Cowboy();
		void draw();
		void setRightUpperArmAngle(float val);
		void setLeftUpperArmAngle(float val);
		void setRightLowerArmAngle(float val);
		void setLeftLowerArmAngle(float val);
		void setRightUpperLegAngle(float val);
		void setLeftUpperLegAngle(float val);
		void setRightLowerLegAngle(float val);
		void setLeftLowerLegAngle(float val);
		bool isStopped();

		void rightLegUpdate(float val);
		void leftLegUpdate(float val);
		void translate();
		void shoot();
		void startShootStance();
		void updateShootStance();

	public:
		GLUquadric* body = nullptr;
		GLUquadric* neck = nullptr;
		GLUquadric* head = nullptr;
		GLUquadric* leftUpperArm= nullptr;
		GLUquadric* leftLowerArm = nullptr;
		GLUquadric* rightUpperArm= nullptr;
		GLUquadric* rightLowerArm = nullptr;

		GLUquadric* leftUpperLeg= nullptr;
		GLUquadric* leftLowerLeg = nullptr;
		GLUquadric* rightUpperLeg = nullptr;
		GLUquadric* rightLowerLeg = nullptr;

		GLUquadric* leftKnee = nullptr;
		GLUquadric* leftShoulder = nullptr;
		GLUquadric* leftElbow = nullptr;
		GLUquadric* leftHand = nullptr;

		GLUquadric* rightKnee = nullptr;
		GLUquadric* rightShoulder = nullptr;
		GLUquadric* rightElbow = nullptr;
		GLUquadric* rightHand = nullptr;


		Mesh* leftBoot;
		Mesh* rightBoot;
		Mesh* hat;
		Mesh* pistol;

	private:
		std::vector<Bullet*> bullets;
		ParticleGenerator* particleGenerator;
		Image* texture;

		float rightUpperArmAngle = 0;
		float leftUpperArmAngle = 0;

		float rightLowerArmAngle = 0;
		float leftLowerArmAngle = 0;

		float rightUpperLegAngle = 0;
		float leftUpperLegAngle = 0;
		
		float rightLowerLegAngle = 0;
		float leftLowerLegAngle = 0;

		float shootingStanceAngle = 0;
		bool spawnBlood = false;

		// Keyframes
		float forwardMotion = 20;
		float steps = 0;
		float animSpeed = 0.05;
		// 5 degrees
		float shootingStanceAnimSpeed = 0.0174532925 * 5;


		bool stopped = false;
		bool shootingStanceStarted = false;
		// Mesh* bullet;
};
