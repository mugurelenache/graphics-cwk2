#include "Bird.h"
#include "Mesh.h"
#include <cmath>

Bird::Bird()
{
	body = gluNewQuadric();
	tail = gluNewQuadric();
	head = gluNewQuadric();
	peak = gluNewQuadric();
}

void Bird::setSpeed(float val)
{
	this->speed = val;
}

void Bird::setMaxAngle(int val)
{
	this->maxAngle = val;
}

void Bird::setRadius(int val)
{
	this->radius = val;
}


void Bird::update()
{
	if(flutter < 0)
	{
		wingAngle--;
		if(wingAngle < -maxAngle)
		{
			flutter = 1;
		}
	}
	else if(flutter > 0)
	{
		wingAngle++;
		if(wingAngle > maxAngle)
		{
			flutter = -1;
		}
	}
}

void Bird::draw()
{
	glPushMatrix();
	{
		glRotatef(rotationAngle += speed * 5/(radius), 0, -1 , 0);
		glTranslatef(radius, 0, 0);

		glPushMatrix();
		{
			float flutterDist = 0.08 * wingAngle * speed;
			if(flutterDist < -10) 
			{
				flutterDist = -10;
			}
			else if(flutterDist > 10)
			{
				flutterDist = 10;
			}

			glTranslatef(0, flutterDist, 0);
			// body
			glPushMatrix();
			{
				gluCylinder(body, 0.5, 0.6, 2.85, 4, 5);
			}
			glPopMatrix();

			// Head
			glPushMatrix();
			{
				glTranslatef(0, 0, 2.85 + 0.7);
				gluSphere(head, 0.8, 5, 5);

				glPushMatrix();
				{
					// Peak
					glRotatef(90, 1, 0, 0);
					glScalef(0.5, 0.5, 0.2);
					glTranslatef(0, 2.0, 0);
					Mesh::pyramid();	
				}
				glPopMatrix();
			}
			glPopMatrix();

			// Left wing
			glPushMatrix();
			{
				glRotatef(-1 * wingAngle * speed, 0, 0, 1);
				glTranslatef(1.75, 0, 1.75 / 2);
				glRotatef(90, 0, 1, 0);
				
				Mesh::plank(0.9, 1.5, 0.15);
				glPushMatrix();
				{	
					glRotatef(0.25 * wingAngle * speed, 1, 0, 0);
					glTranslatef(0.25, 0 , 1.5 + 0.9);
					glRotatef(15, 0, 1, 0);
				
					Mesh::plank(0.8, 1.2, 0.18);
				}
				glPopMatrix();
			}
			glPopMatrix();

			// Right wing
			glPushMatrix();
			{
				glRotatef(wingAngle * speed, 0, 0, 1);
				glTranslatef(-1.75, 0, 1.75 / 2);
				glRotatef(-90, 0, 1, 0);
				// Angle
				Mesh::plank(0.9, 1.5, 0.15);
				glPushMatrix();
				{
					glRotatef(0.25 * wingAngle * speed, 1, 0, 0);
					glTranslatef(-0.25, 0 , 1.5 + 0.9);
					glRotatef(-15, 0, 1, 0);
					Mesh::plank(0.8, 1.2, 0.18);
				}
				glPopMatrix();
			}
			glPopMatrix();



			// Tail
			glPushMatrix();
			{
				glRotatef(0.15 * wingAngle * speed, 1, 0, 0);
				glTranslatef(0, 0, -1);
				glRotatef(90, 1, 0, 0);
				glScalef(1.25, 1.5, 0.25);
				Mesh::pyramid();	
			}
			glPopMatrix();
		}
		glPopMatrix();
	}
	glPopMatrix();
}

Bird::~Bird()
{
	gluDeleteQuadric(body);
	gluDeleteQuadric(tail);
	gluDeleteQuadric(head);
	gluDeleteQuadric(peak);
}
