#include "ParticleGenerator.h"
#include "Image.h"

ParticleGenerator::ParticleGenerator(Image* texture)
{
	this->texture = texture;
}

ParticleGenerator::~ParticleGenerator()
{
}

void ParticleGenerator::drawParticles()
{
	for(size_t i = 0; i < particles.size(); i++)
	{
		particles[i]->draw();
	}
}

void ParticleGenerator::generate(int n, float x, float y, float z, float sx, float sy, float sz, bool random, bool isBlood)
{
	if(!spawned)
	{
		for(int i = 0; i < n; i++)
		{

			if(random)
			{
				if(isBlood)
				{
					particles.push_back(new Particle(texture, sx,sy,sz, x,y,z, 0,0,0, rand()%180, rand()%180, rand()%180));
				}
				else
				{
					particles.push_back(new Particle(texture, sx,sy,sz, x,y,z, 0,0,0, 0, 0, 0));
				}
			}
			else
			{
				particles.push_back(new Particle(texture, sx,sy,sz, x,y,z, 0,0,0, 0, 0,0));
			}
		}

		spawned = true;
	}
}
