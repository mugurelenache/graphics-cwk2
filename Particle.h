#pragma once
#include "Mesh.h"

class Particle
{
	public:
		Particle(Image* texture, float scaleX, float scaleY, float scaleZ, float x, float y, float z, float dirX, float dirY, float dirZ, float rotX, float rotY, float rotZ);
		~Particle();
		void draw();

	public:
		Image* texture;
		float scale[3] = { 1 };
		float position[3] = { 0 };
		float direction[3] = { 0 };
		float rot[3] = { 0 };
};
