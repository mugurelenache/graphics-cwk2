#include "Cowboy.h"
#include <cmath>
#include <iostream>
#include "ParticleGenerator.h"

Cowboy::Cowboy(Image* texture, Mesh* leftBoot, Mesh* rightBoot, Mesh* hat, Mesh* pistol, ParticleGenerator* particleGenerator)
{
	this->leftBoot = leftBoot;
	this->rightBoot = rightBoot;
	this->hat = hat;
	this->pistol = pistol;
	this->particleGenerator = particleGenerator;
	this->texture = texture;

	body = gluNewQuadric();
	neck = gluNewQuadric();
	head = gluNewQuadric();
	gluQuadricTexture(head, GLU_TRUE);
	gluQuadricNormals(head, GLU_SMOOTH);

	leftUpperArm=gluNewQuadric(); 
	leftLowerArm = gluNewQuadric();
	rightUpperArm= gluNewQuadric();
	rightLowerArm = gluNewQuadric();

	leftUpperLeg= gluNewQuadric();
	leftLowerLeg = gluNewQuadric();
	rightUpperLeg = gluNewQuadric();
	rightLowerLeg = gluNewQuadric();

	leftHand = gluNewQuadric();
	rightHand = gluNewQuadric();

	leftKnee = gluNewQuadric();
	rightKnee = gluNewQuadric();

	leftShoulder = gluNewQuadric();
	rightShoulder = gluNewQuadric();

	leftElbow = gluNewQuadric();
	rightElbow = gluNewQuadric();
}

Cowboy::~Cowboy()
{
	gluDeleteQuadric(body);
	gluDeleteQuadric(neck);
	gluDeleteQuadric(head);

	gluDeleteQuadric(leftUpperArm);
	gluDeleteQuadric(leftLowerArm);
	gluDeleteQuadric(rightUpperArm);
	gluDeleteQuadric(rightLowerArm );

	gluDeleteQuadric(leftUpperLeg);
	gluDeleteQuadric(leftLowerLeg);
	gluDeleteQuadric(rightUpperLeg);
	gluDeleteQuadric(rightLowerLeg);


	gluDeleteQuadric(leftHand);
	gluDeleteQuadric(rightHand);
	gluDeleteQuadric(leftKnee);
	gluDeleteQuadric(rightKnee);
	gluDeleteQuadric(leftShoulder);
	gluDeleteQuadric(rightShoulder);
	gluDeleteQuadric(leftElbow);
	gluDeleteQuadric(rightElbow);

	leftBoot = nullptr;
	rightBoot = nullptr;
	hat = nullptr;
	pistol = nullptr;
}

bool Cowboy::isStopped()
{
	return stopped;
}

void Cowboy::startShootStance()
{
	if(stopped)
	{	
		if(!shootingStanceStarted)
		{
			setRightUpperArmAngle(0);
			shootingStanceStarted = true;
		}
	}
}

void Cowboy::updateShootStance()
{
	if(shootingStanceStarted)
	{
		// pi/2 rad
		if(shootingStanceAngle < 1.57)
		{
			shootingStanceAngle += shootingStanceAnimSpeed;
			setRightLowerArmAngle(sin(shootingStanceAngle) * 2);
		}
	}
}

void Cowboy::shoot()
{
	if(shootingStanceStarted)
	{
		bullets.push_back(new Bullet(0, 0, -1));

		// Bullet translate etc
	}
}

void Cowboy::rightLegUpdate(float val)
{
	setRightUpperArmAngle(val);
	setRightLowerArmAngle(fabs(val));
	setRightUpperLegAngle(val);
	if(val < 0)
	{
		setRightLowerLegAngle(val);
	}
	else
	{
		setRightLowerLegAngle(-val);
	}
}

void Cowboy::leftLegUpdate(float val)
{
	setLeftUpperLegAngle(val);
	setLeftUpperArmAngle(val);
	setLeftLowerArmAngle(fabs(val));
	if(val < 0)
	{
		setLeftLowerLegAngle(val);
	}
	else
	{
		setLeftLowerLegAngle(-val);
	}
}

void Cowboy::translate()
{
	// If value
	if(!stopped)
	{
		if(forwardMotion > 0)
		{
			forwardMotion-= animSpeed;
			rightLegUpdate(sin(forwardMotion));
			leftLegUpdate(cos(90 + forwardMotion));
		}
		else
		{
			float sine = sin(forwardMotion);

			if(sine >= -0.1 && sine <= 0.1)
			{
				stopped = true;
			}
			else
			{
				forwardMotion -= animSpeed;
				rightLegUpdate(sine);
				leftLegUpdate(cos(90 + forwardMotion));
			}
		}
	}
}

void Cowboy::setRightUpperArmAngle(float val)
{
	this->rightUpperArmAngle = val;
}

void Cowboy::setLeftUpperArmAngle(float val)
{
	this->leftUpperArmAngle = val;
}

void Cowboy::setRightLowerArmAngle(float val)
{
	this->rightLowerArmAngle = val;
}

void Cowboy::setLeftLowerArmAngle(float val)
{
	this->leftLowerArmAngle = val;
}

void Cowboy::setRightUpperLegAngle(float val)
{
	this->rightUpperLegAngle = val;
}

void Cowboy::setLeftUpperLegAngle(float val)
{
	this->leftUpperLegAngle = val;
}

void Cowboy::setRightLowerLegAngle(float val)
{
	this->rightLowerLegAngle = val;
}

void Cowboy::setLeftLowerLegAngle(float val)
{
	this->leftLowerLegAngle = val;
}





void Cowboy::draw()
{
	// Body
	Materials::defaultM.set();
	glPushMatrix();

	if(forwardMotion > 0)
	{
		glTranslatef(0, steps--  * animSpeed, 0);
	}
	else
	{
		glTranslatef(0, steps* animSpeed, 0);
	}
	{
		glPushMatrix();

		{
			glPushMatrix();

			Materials::brass.set();
			glTranslatef(0, 0, -2.5); // center it
			gluCylinder(body, 1, 1.2, 5, 8, 5);
			Materials::defaultM.set();

			// Left upper leg
			{
				glPushMatrix();

				glRotatef(this->leftUpperLegAngle * 45, 1, 0, 0);
				glTranslatef(0.5, 0, -2);
				gluCylinder(leftUpperLeg, 0.3, 0.3, 3, 6, 6);
				{
					// Left Knee
					glPushMatrix();

					gluSphere(leftKnee, 0.3, 6, 6);
					// Left lower leg
					{
						glPushMatrix();

						glRotatef(this->leftLowerLegAngle * -45, 1, 0, 0);
						glTranslatef(-0, 0, -2);
						gluCylinder(leftLowerLeg, 0.25, 0.25, 2, 6, 6);

						// Left shoe
						{
							glPushMatrix();

							glScalef(0.08, 0.08, 0.08);
							glRotatef(90, 1, 0, 0);
							glTranslatef(-1.5, 0, 0.5);
							leftBoot->draw();

							glPopMatrix();
						}

						glPopMatrix();
					}

					glPopMatrix();
				}

				glPopMatrix();
			}

			// Right upper leg
			{
				glPushMatrix();

				glRotatef(this->rightUpperLegAngle * 45, 1, 0, 0);
				glTranslatef(-0.5, 0, -2);
				gluCylinder(rightUpperLeg, 0.3, 0.3, 3, 6, 6);
				{
					// Right Knee
					glPushMatrix();

					gluSphere(rightKnee, 0.3, 6, 6);

					// Right lower leg
					{
						glPushMatrix();

						glRotatef(this->rightLowerLegAngle * -45, 1, 0, 0);
						glTranslatef(-0, 0, -2);
						gluCylinder(rightLowerLeg, 0.25, 0.25, 2, 6, 6);

						// Right shoe
						{
							glPushMatrix();

							glScalef(0.08, 0.08, 0.08);
							glRotatef(90, 1, 0, 0);
							glTranslatef(1.5, 0, 0.5);
							rightBoot->draw();

							glPopMatrix();
						}

						glPopMatrix();
					}

					glPopMatrix();
				}

				glPopMatrix();
			}

			glPopMatrix();
		}

		// Pistol
		{
			glPushMatrix();
			glTranslatef(1.2, 0, 0.2);
			glScalef(0.03, 0.03, 0.03);
			pistol->draw();
			glPopMatrix();
		}

		// Neck
		{
			glPushMatrix();

			glTranslatef(0, 0, 2.5);
			glScalef(0.3, 0.3, 0.5);
			gluCylinder(neck, 1, 1, 1, 6, 3);

			{
				glPushMatrix();

				glTranslatef(0, 0, 2.5);
				
				this->texture->load();
				gluSphere(head, 2, 5, 5);
				this->texture->unload();

				{
					glPushMatrix();

					glRotatef(90, 1, 0, 0);
					glTranslatef(0.1, 1.9, 0);
					glScalef(1.5, 1.5, 1.5);
					hat->draw();

					glPopMatrix();
				}

				glPopMatrix();
			}

			glPopMatrix();		
		}

		// Right shoulder
		{
			glPushMatrix();

			glTranslatef(-1.4, 0, 2.2);
			gluSphere(rightShoulder, 0.3, 6, 6);

			// Upper arm
			{
				glPushMatrix();

				glRotatef(this->rightUpperArmAngle * -45, 1, 0, 0);
				glRotatef(-180 + 15, 0, 1, 0);
				gluCylinder(rightUpperArm, 0.3, 0.3, 2, 6, 6);

				// Elbow
				{
					glPushMatrix();

					glTranslatef(-0, 0, 2 + 0.35 / 2.0);
					gluSphere(rightElbow, 0.35, 6, 6);

					// arm 
					{
						glPushMatrix();

						glRotatef(this->rightLowerArmAngle * 45, 1, 0, 0);
						gluCylinder(rightLowerArm, 0.25, 0.25, 2, 6, 6);

						// hand
						{
							glPushMatrix();

							glTranslatef(0, 0, 2.2);
							glRotatef(90, 0, 0, -1);
							gluSphere(rightHand, 0.4, 6, 6);

							// Pistol
							{
								glPushMatrix();
								glRotatef(180, 1, 1, 0);
								glTranslatef(0, 0, -0.80);
								glScalef(0.05, 0.05, 0.05);
								glRotatef(2.2, 0, 1, 0);
								pistol->draw();

								for(auto& bullet : bullets)
								{
									glPushMatrix();
									glTranslatef(0, 0, -15);
									bullet->updateBulletMotion();

									if(bullet->keyframes == 1450)
									{
										spawnBlood = true;
									};
									glPopMatrix();
								}


								if(spawnBlood)
								{
									particleGenerator->generate(6, 0,0,0, 40, 40, 40, true, false); 
									glPushMatrix();
									glTranslatef(0,0,-1350);
									particleGenerator->drawParticles();
									glPopMatrix();
								}


								for(size_t i = 0; i < bullets.size(); i++)
								{
									// HIT SOMETHING / OUT OF VIS
									if(bullets[i]->keyframes > 1475)
									{
										delete(bullets[i]);
										bullets.erase(bullets.begin() + i);
									}
								}

								glPopMatrix();
							}
							glPopMatrix();
						}
						glPopMatrix();
					}
					glPopMatrix();
				}
				glPopMatrix();
			}
			glPopMatrix();
		}

		// Left shoulder
		{
			glPushMatrix();

			glTranslatef(1.4, 0, 2.2);
			gluSphere(leftShoulder, 0.3, 6, 6);

			// Upper arm
			{
				glPushMatrix();

				glRotatef(-180 - 15, 0, 1, 0);

				glRotatef(leftUpperArmAngle * 45, 1, 0, 0);
				gluCylinder(leftUpperArm, 0.3, 0.3, 2, 6, 6);

				// Elbow
				{
					glPushMatrix();

					glTranslatef(-0, 0, 2 + 0.35 / 2.0);
					gluSphere(leftElbow, 0.35, 6, 6);

					// Lower arm 
					{
						glPushMatrix();

						glRotatef(leftLowerArmAngle* 45, 1, 0, 0);
						gluCylinder(rightLowerArm, 0.25, 0.25, 2, 6, 6);

						// hand
						{
							glPushMatrix();

							glTranslatef(0, 0, 2.2);
							gluSphere(leftHand, 0.4, 6, 6);

							glPopMatrix();
						}

						glPopMatrix();
					}

					glPopMatrix();
				}

				glPopMatrix();
			}

			glPopMatrix();
		}

		glPopMatrix();
	}
	glPopMatrix();
}
