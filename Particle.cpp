#include "Particle.h"

Particle::Particle(Image* texture, float scaleX, float scaleY, float scaleZ, float x, float y, float z, float dirX, float dirY, float dirZ, float rotX, float rotY, float rotZ)
{
	this->texture = texture;

	scale[0] = scaleX;
	scale[1] = scaleY;
	scale[2] = scaleZ;

	position[0] = x;
	position[1] = y;
	position[2] = z;

	direction[0] = dirX;
	direction[1] = dirY;
	direction[2] = dirZ;

	rot[0] = rotX;
	rot[1] = rotY;
	rot[2] = rotZ;
	
}

Particle::~Particle()
{
}

void Particle::draw()
{
	glPushMatrix();
		glDisable(GL_DEPTH_TEST);
		glNormal3f(1, 0, 0);
		glTranslatef(position[0], position[1], position[2]);
		glScalef(scale[0], scale[1], scale[2]);
		glColor4f(1, 1, 1, 0);
		texture->load();
		glRotatef(rot[0], 1, 0, 0);
		glRotatef(rot[1], 0, 1, 0);
		glRotatef(rot[2], 0, 0, 1);
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0);
			glVertex3f(1.0,-1.0, 1.0);
			glTexCoord2f(1, 0);
			glVertex3f(1.0,-1.0,-1.0);
			glTexCoord2f(1, 1);
			glVertex3f(1.0, 1.0,-1.0);
			glTexCoord2f(0, 1);
			glVertex3f(1.0, 1.0, 1.0);
		glEnd();
		glColor4f(1, 1, 1, 1);
		texture->unload();
		glEnable(GL_DEPTH_TEST);
	glPopMatrix();
}
