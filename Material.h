#pragma once
#include <GL/glu.h>

struct Material
{
	GLfloat ambient[4];
	GLfloat diffuse[4];
	GLfloat specular[4];
	GLfloat shininess;

	void set();
	void unset();
};

namespace Materials 
{
	extern Material brass; 
	extern Material plank;
	extern Material plankV1;
	extern Material whiteShiny;
	extern Material defaultM;
	extern Material cactus;
}
