#pragma once
#include <QGLWidget>
#include <QKeyEvent>
#include <GL/glut.h>
#include "Camera.h"
#include "Mesh.h"
#include "Image.h"
#include "Bird.h"
#include "Cowboy.h"
#include "ParticleGenerator.h"

class RendererWidget : public QGLWidget
{
	Q_OBJECT

	public:
		RendererWidget(QWidget *parent);

		public slots:
			void updateBirdWingAngle(int val);	
			void updateBirdSpeed(int val);	
			void updateBirdAnim();	
			void updateBirdCircleRadius(int val);
			void updateCowboysMotion();
			void updateDustCount(int val);
			void updateWantedLevel(int val);
			void updateWindSpeed(int val);
			void hideTumbleweed();
			void showTumbleweed();
			void shoot();


	protected:
		// called when OpenGL context is set up
		void initializeGL();
		// called every time the widget is resized
		void resizeGL(int w, int h);
		// called every time the widget needs painting
		void paintGL();
		void keyPressEvent(QKeyEvent* event);

		void drawSkysphere();
		void drawBird();
		void drawCactus();
		void drawLight();
		void drawTerrain();
		void drawBulls();
		void drawHorses();
		void drawGallows();
		void drawStackedCrateMesh();
		void drawCemetery();
		void drawWantedPoster();
		void drawSaloonMesh3();
		void drawSaloonMesh2();
		void drawVentMesh();
		void drawWstHouseMesh();
		void drawSaloonMesh();
		void drawBlacksmMesh();
		void drawCarriageMesh();
		void drawBarrelMesh();
		void drawCrateMesh();
		void drawRocks();
		void drawCowboys();
		void drawCactuses();
		void drawTumbleweed();
		void drawDustParticles();
		void drawMap();

	private:
		Camera camera;
		int angle = 0;
		int rotAngle = 0;
		int dustCount = 5000;
		int posterCount = 50;
		float windSpeed = 2;
		bool shouldDrawTumbleweed = true;

		Image* moiTex;
		Image* earthTex;
		Image* terrainTex;
		Image* skyTex;
		Image* tumbleweedTex;
		Image* wantedTex;
		Image* cliffTex;

		Mesh revolverMesh;
		Mesh saloonMesh;
		Mesh hatMesh;
		Mesh barrelMesh;
		Mesh bootMesh1;
		Mesh bootMesh2;
		Mesh blacksmMesh;
		Mesh carriageMesh;
		Mesh wstHouseMesh;
		Mesh ventMesh;
		Mesh crate;
		Mesh saloonMesh2;
		Mesh saloonMesh3;
		Mesh test;
		Mesh bull;
		Mesh horse;
		Mesh cross;
		Mesh gallows;
		Mesh haycart;
		Mesh rock;

		Bird* bird;
		Cowboy* cowboyLeft;
		Cowboy* cowboyRight;

		ParticleGenerator* bloodGenerator;
		ParticleGenerator* dustGenerator;

		float tumbleweedInit[6];
		float dustParticles[5000][3];
		float cactusInit[30][9];
		float posterInit[5000][3];
		float tumbleSpeed = 0.2;
};
