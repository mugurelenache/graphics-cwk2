#include <QGLWidget>
#include <GL/glu.h>
#include <iostream>
#include "RendererWidget.h"
#include "Material.h"
#include "Mesh.h"
#include <time.h>

RendererWidget::RendererWidget(QWidget *parent) : QGLWidget(parent)
{
	camera = Camera(0, 45, 100, 
			0, 15, 0,
		   	0.01f, 1500.0f, 1024, 768 + 112);
	bird = new Bird();
	setFocusPolicy(Qt::StrongFocus);
	setFocus();
}

void RendererWidget::initializeGL()
{
	glClearColor(0.3, 0.3, 0.3, 1.0);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);
	glShadeModel(GL_SMOOTH);
	moiTex = new Image("./textures/Moi.ppm");
	wantedTex = new Image("./textures/wanted.jpg");
	earthTex = new Image("./textures/earth.ppm");
	terrainTex = new Image("./textures/dirt.jpg");
	skyTex = new Image("./textures/sky_texture.jpg");
	tumbleweedTex = new Image("./textures/tumbleweed.png");
	cliffTex = new Image("./textures/cliff.jpg");

	saloonMesh.loadObj("./meshes/saloon_00/saloon_00.obj", new Image("./meshes/saloon_00/saloon.png"));


	hatMesh.loadObj("./meshes/hat.obj", new Image("./textures/brown.jpg"));
	revolverMesh.loadObj("./meshes/revolver/revolver.obj", new Image("./meshes/revolver/wood.jpg"));
	barrelMesh.loadObj("./meshes/barrel/barrel.obj", new Image("./meshes/barrel/wood33L.jpg"));
	blacksmMesh.loadObj("./meshes/blacksmith_00/blacksmith_00.obj", new Image("./meshes/blacksmith_00/btxt.png"));
	carriageMesh.loadObj("./meshes/carriage/carriage.obj", new Image("./meshes/carriage/carriage_tex.jpg"));
	wstHouseMesh.loadObj("./meshes/western_house_00/western_house_00.obj", new Image("./meshes/western_house_00/wstHouseTxt.png"));
	ventMesh.loadObj("./meshes/vent/vent_00.obj", new Image("./meshes/vent/venttxt.png"));

	// Left
	bootMesh1.loadObj("./meshes/boot/boot1.obj", new Image("./textures/brown.jpg"));

	// Right
	bootMesh2.loadObj("./meshes/boot/boot2.obj", new Image("./textures/brown.jpg"));

	crate.loadObj("./meshes/crate/crate1.obj", new Image("./meshes/crate/T_crate1_D.png"));
	saloonMesh2.loadObj("./meshes/bs/saloon.obj", new Image("./meshes/bs/dif.jpg"));
	saloonMesh3.loadObj("./meshes/saloon3/saloon.obj", new Image("./meshes/saloon3/storeDiffuseMap.jpg"));

	bull.loadObj("./meshes/bull/Bull.obj", new Image("./meshes/bull/texture.png"));
	horse.loadObj("./meshes/horse/horse.obj");
	cross.loadObj("./meshes/cross/cross.obj");
	gallows.loadObj("./meshes/gallows/gallows.obj", new Image("./meshes/haycart/Wood.png"));
	haycart.loadObj("./meshes/haycart/HayCart.obj", new Image("./meshes/haycart/Wood.png"));
	rock.loadObj("./meshes/rock/rock.obj", new Image("./textures/cliff.jpg"));

	bloodGenerator = new ParticleGenerator(new Image("./textures/blood.png"));
	dustGenerator = new ParticleGenerator(new Image("./textures/Moi.ppm"));

	cowboyLeft = new Cowboy(moiTex, &bootMesh1, &bootMesh2, &hatMesh, &revolverMesh, bloodGenerator);
	cowboyRight = new Cowboy(moiTex, &bootMesh1, &bootMesh2, &hatMesh, &revolverMesh, bloodGenerator);


	dustGenerator->generate(5, 0, 0, 0, 15, 15, 15, true, false);
	srand(time(NULL));

	// Generate dust particles positions
	for(int i = 0; i < 5000; i++)
	{
		for(int j = 0; j < 3; j++)
		{
			dustParticles[i][j] = rand() % 600 - 300;
		}
	}

	// INCREASE WANTED LEVEL
	for(int i = 0; i < 5000; i++)
	{
		for(int j = 0; j < 2; j++)
		{
			posterInit[i][j] = rand() % 400 - 200;
		}

		posterInit[i][2] = rand() % 180 - 90;	
	}

	// Generate tumbleweed poss
	for(int i = 0; i < 30; i++)
	{
		tumbleweedInit[i] = rand() % 200 - 100;
	}


	// Generate cactus positions
	for(int i = 0; i < 30; i++)
	{
		for(int j = 0; j < 2; j++)
		{
			if(j == 1)
			{
				float y = rand() % 700 - 100;
				while(y < 200 && y > -100)
				{
					y = rand() % 1000 - 400;
				}

				cactusInit[i][j] = -y;
			}
			else
			{
				cactusInit[i][j] = rand() % 700 - 100;
			}
		}
		for(int j = 2; j < 9; j++)
		{
			cactusInit[i][j] = rand() % 20;
		}
	}
}

void RendererWidget::hideTumbleweed()
{
	this->shouldDrawTumbleweed = false;
	this->repaint();
}

void RendererWidget::showTumbleweed()
{
	this->shouldDrawTumbleweed = true;
	this->repaint();
}

void RendererWidget::shoot()
{
	cowboyLeft->shoot();
	cowboyLeft->startShootStance();

	this->repaint();
}

void RendererWidget::keyPressEvent(QKeyEvent* event)
{
	switch(event->key())
	{
		case Qt::Key_W:
			{
				camera.translate(0, 0, -0.5f);
				break;
			}
		case Qt::Key_D:
			{
				camera.translate(0.5f, 0, 0);
				break;
			}
		case Qt::Key_A:
			{
				camera.translate(-0.5f, 0, 0);
				break;
			}
		case Qt::Key_S:
			{
				camera.translate(0, 0, 0.5f);
				break;
			}
		case Qt::Key_E:
			{
				camera.translate(0, 0.5f, 0); 
				break;
			}
		case Qt::Key_Q:
			{
				camera.translate(0, -0.5f, 0); 
				break;
			}
		case Qt::Key_Left:
			{
				camera.translateOrientation(-0.5f, 0, 0);
				break;
			}
		case Qt::Key_Right:
			{
				camera.translateOrientation(0.5f, 0, 0);
				break;
			}
		case Qt::Key_Down:
			{
				camera.translateOrientation(0, -0.5f, 0);
				break;
			}
		case Qt::Key_Up:
			{
				camera.translateOrientation(0, 0.5f, 0);
				break;
			}
		case Qt::Key_Space:
			{
				cowboyLeft->shoot();
				cowboyLeft->startShootStance();
				break;
			}
	}
}

void RendererWidget::updateDustCount(int val)
{
	this->dustCount = val;
	this->repaint();
}

void RendererWidget::drawDustParticles()
{
	if(windSpeed > 0)
	{
		for (int i = 0; i < dustCount; i++)
		{
			Materials::defaultM.set();
			glColor3f(0.25, 0.45, 0.25);
			glBegin(GL_LINES);
			glVertex3f(dustParticles[i][0], dustParticles[i][1], dustParticles[i][2]);
			glVertex3f(dustParticles[i][0]+= 1 * windSpeed/2.0, dustParticles[i][1], dustParticles[i][2]);
			if(dustParticles[i][0] > 300)
			{
				dustParticles[i][0] = -300;
			}
			glColor3f(1,1,1);
			glEnd();
			Materials::defaultM.unset();
		}
	}
}

void RendererWidget::drawRocks()
{
	glPushMatrix();
	{
		glDisable(GL_LIGHTING);
		glTranslatef(-500, -50, 350);
		glScalef(10, 10, 10);
		rock.draw();
		glEnable(GL_LIGHTING);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glDisable(GL_LIGHTING);
		glTranslatef(-500, -50, 0);
		glScalef(10, 10, 10);
		rock.draw();
		glEnable(GL_LIGHTING);
	}
	glPopMatrix();
	glPushMatrix();
	{
		glDisable(GL_LIGHTING);
		glTranslatef(-500, -50, -250);
		glScalef(10, 10, 10);
		rock.draw();
		glEnable(GL_LIGHTING);
	}
	glPopMatrix();
	
	glPushMatrix();
	{
		glDisable(GL_LIGHTING);
		glTranslatef(-300, -50, -500);
		glScalef(10, 10, 10);
		glRotatef(90, 0, 1, 0);
		rock.draw();
		glEnable(GL_LIGHTING);
	}
	glPopMatrix();
	glPushMatrix();
	{
		glDisable(GL_LIGHTING);
		glTranslatef(0, -50, -500);
		glScalef(10, 10, 10);
		glRotatef(90, 0, 1, 0);
		rock.draw();
		glEnable(GL_LIGHTING);
	}
	glPopMatrix();
	glPushMatrix();
	{
		glDisable(GL_LIGHTING);
		glTranslatef(300, -50, -500);
		glScalef(10, 10, 10);
		glRotatef(90, 0, 1, 0);
		rock.draw();
		glEnable(GL_LIGHTING);
	}
	glPopMatrix();
	glPushMatrix();
	{
		glDisable(GL_LIGHTING);
		glTranslatef(0, -50, -500);
		glScalef(10, 10, 10);
		glRotatef(90, 0, 1, 0);
		rock.draw();
		glEnable(GL_LIGHTING);
	}
	glPopMatrix();
	glPushMatrix();
	{
		glDisable(GL_LIGHTING);
		glTranslatef(500, -50, -250);
		glScalef(10, 10, 10);
		rock.draw();
		glEnable(GL_LIGHTING);
	}
	glPopMatrix();
	glPushMatrix();
	{
		glDisable(GL_LIGHTING);
		glTranslatef(500, -50, 0);
		glScalef(10, 10, 10);
		rock.draw();
		glEnable(GL_LIGHTING);
	}
	glPopMatrix();
	glPushMatrix();
	{
		glDisable(GL_LIGHTING);
		glTranslatef(600, -50, 0);
		glScalef(10, 10, 10);
		glRotatef(60, 0, 0, 1);
		rock.draw();
		glEnable(GL_LIGHTING);
	}
	glPopMatrix();
	glPushMatrix();
	{
		glDisable(GL_LIGHTING);
		glTranslatef(500, -50, 200);
		glScalef(10, 10, 10);
		rock.draw();
		glEnable(GL_LIGHTING);
	}
	glPopMatrix();
}


void RendererWidget::resizeGL(int w, int h)
{
	// set the viewport to the entire widget
	glViewport(0, 0, w, h);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	glEnable(GL_TEXTURE_2D);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	GLfloat light_pos[] = {1., 10.5, 1., 0.};	
	glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
	glLightf (GL_LIGHT0, GL_SPOT_CUTOFF, 300.);

	camera.resize(w, h);
} 

void RendererWidget::updateBirdSpeed(int val)
{
	this->bird->setSpeed(val / 100.0);
	this->repaint();
}

void RendererWidget::updateBirdCircleRadius(int val)
{
	this->bird->setRadius(val);
	this->repaint();
}

void RendererWidget::updateBirdWingAngle(int val)
{
	this->bird->setMaxAngle(val);
	this->repaint();
}

void RendererWidget::updateBirdAnim()
{
	this->bird->update();
	this->repaint();
}

void RendererWidget::updateCowboysMotion()
{
	this->cowboyLeft->translate();
	this->cowboyLeft->updateShootStance();

	this->cowboyRight->translate();
	this->cowboyRight->updateShootStance();

	this->repaint();
}

void RendererWidget::drawSkysphere()
{	
	glPushMatrix();
	{
		glDisable(GL_LIGHTING);
		Materials::whiteShiny.set();
		skyTex->load();
		glRotatef(75, 1, 0, 0);
		glScalef(1000, 1000, 1000);
		Mesh::sphere();
		skyTex->unload();
		glEnable(GL_LIGHTING);
	}
	glPopMatrix();
}

void RendererWidget::drawBird()
{
	glPushMatrix();
	{
		glTranslatef(0, 35, 0);
		bird->draw();
	}
	glPopMatrix();
}

void RendererWidget::drawCactus()
{
	glPushMatrix();
	{
		glTranslatef(-75, 0.5, -100);
		Mesh::cactus(15, 5, 3, 3, 10, true, true);
	}
	glPopMatrix();
}

void RendererWidget::drawLight()
{
	glPushMatrix();
	{
		glLoadIdentity();
		GLfloat light_pos[] = {1., 5., 0., 0.};
		glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
		glLightf (GL_LIGHT0, GL_SPOT_CUTOFF,15.);
	}
	glPopMatrix();
}

void RendererWidget::drawTerrain()
{
	glPushMatrix();
	{
		glScalef(500,1,500);
		terrainTex->load();
		Mesh::terrain();
		terrainTex->unload();
	}

	glPopMatrix();
}

void RendererWidget::drawBulls()
{
	glPushMatrix();
	{
		glTranslatef(270, 0, -100);
		glPushMatrix();
		{
			glTranslatef(0, 6, 0);
			Mesh::fence(15, 0.5, 0.5, 6);
			glTranslatef(30, 0, 0);
			glRotatef(90, 0, 1, 0);

			Mesh::fence(15, 0.5, 0.5, 6);
			glTranslatef(30, 0, 0);
			glRotatef(90, 0, 1, 0);

			Mesh::fence(15, 0.5, 0.5, 6);

			glTranslatef(30, 0, 0);
			glRotatef(90, 0, 1, 0);

			Mesh::fence(15, 0.5, 0.5, 6);

			glTranslatef(15, -6, -15);

			glPushMatrix();
			glScalef(.5, .5, .5);
			horse.draw();
			glPopMatrix();

			glTranslatef(5, 0, 5);

			glPushMatrix();
			glScalef(0.5, 0.5, 0.5);
			horse.draw();
			glPopMatrix();

		}
		glPopMatrix();
	}
	glPopMatrix();
}

void RendererWidget::drawHorses()
{	
	// Horses 
	glPushMatrix();
	{
		glTranslatef(70, 1, -150);
		glPushMatrix();
		{
			glTranslatef(0, 6, 0);
			Mesh::fence(15, 0.5, 0.5, 6);
			glTranslatef(30, 0, 0);
			glRotatef(90, 0, 1, 0);

			Mesh::fence(15, 0.5, 0.5, 6);
			glTranslatef(30, 0, 0);
			glRotatef(90, 0, 1, 0);

			Mesh::fence(15, 0.5, 0.5, 6);

			glTranslatef(30, 0, 0);
			glRotatef(90, 0, 1, 0);

			Mesh::fence(15, 0.5, 0.5, 6);

			glTranslatef(15, -6, -15);

			glPushMatrix();
			glScalef(1.7, 1.7, 1.7);
			bull.draw();
			glPopMatrix();

			glTranslatef(5, 0, 5);

			glPushMatrix();
			glScalef(1.7, 1.7, 1.7);
			bull.draw();
			glPopMatrix();

		}
		glPopMatrix();
	}
	glPopMatrix();


}

void RendererWidget::drawGallows()
{
	glPushMatrix();
	{
		glTranslatef(-240, 0, -150);
		glScalef(0.1, 0.1, 0.1);
		Materials::plank.set();
		glDisable(GL_LIGHTING);
		gallows.draw();
		glEnable(GL_LIGHTING);
		Materials::plank.unset();
	}
	glPopMatrix();

}

void RendererWidget::drawCemetery()
{
	glPushMatrix();
	{
		glTranslatef(-220, 0, -150);
		for(int i = 0; i < 5; i++)
		{
			for(int j = 0; j < 5; j++)
			{
				glPushMatrix();
				{
					glTranslatef(i * 10, 0, j * 10);
					cross.draw();
				}
				glPopMatrix();
			}
		}
	}
	glPopMatrix();
}

void RendererWidget::updateWantedLevel(int val)
{
	this->posterCount = val;
	this->repaint();
}

void RendererWidget::updateWindSpeed(int val)
{
	this->windSpeed = val;
	this->repaint();
}

void RendererWidget::drawMap()
{
	Materials::whiteShiny.set();
	glPushMatrix();
	glTranslatef(10, 15, -108);
	glRotatef(90, 1, 0, 0);
	glRotatef(10, 0, 1, 0);
	glScalef(8, 6, 6);
	earthTex->load();
	Mesh::wanted();
	earthTex->unload();
	glPopMatrix();

}

void RendererWidget::drawWantedPoster()
{
	// Draw the wanted poster
	for(int i = 0; i < posterCount; i++)
	{
		glPushMatrix();
		{
			Materials::whiteShiny.set();
			glPushMatrix();
			glTranslatef(posterInit[i][0], 0.1, posterInit[i][1]);
			glRotatef(posterInit[i][0], 0, 1, 0);
			glScalef(1.5, 1.5, 1.5);
			wantedTex->load();
			Mesh::wanted();
			{
				glPushMatrix();
				{
					glScalef(0.33, 1, 0.33);
					glTranslatef(0.0, 0.05, -0.1);
					moiTex->load();
					Mesh::wanted();
				}
				glPopMatrix();
			}
			glPopMatrix();
			wantedTex->unload();
		}
		glPopMatrix();
	}

	glPushMatrix();
	{
		Materials::whiteShiny.set();
		glPushMatrix();
		glTranslatef(0.0, 0.1, 0);
		glScalef(1.5, 1.5, 1.5);
		wantedTex->load();
		Mesh::wanted();
		{
			glPushMatrix();
			glScalef(0.33, 1, 0.33);
			glTranslatef(0.0, 0.05, -0.1);
			moiTex->load();
			Mesh::wanted();
			glPopMatrix();
		}
		glPopMatrix();
	}
	glPopMatrix();

}

void RendererWidget::drawSaloonMesh3()
{
	glPushMatrix();
	{
		glTranslatef(-110, 0, -100);
		glRotatef(90, 0, 1, 0);
		saloonMesh3.draw();
	}
	glPopMatrix();
}

void RendererWidget::drawSaloonMesh2()
{
	glPushMatrix();
	{
		glTranslatef(0, 0.5, -130);
		saloonMesh2.draw();
	}
	glPopMatrix();
}

void RendererWidget::drawVentMesh()
{
	glPushMatrix();
	{
		glTranslatef(120, 0, -100);
		glScalef(100, 100, 100);
		glRotatef(-90, 0, 1, 0);
		ventMesh.draw();
	}
	glPopMatrix();
}

void RendererWidget::drawWstHouseMesh()
{
	glPushMatrix();
	{
		glTranslatef(90, 0, -100);
		glScalef(100, 100, 100);
		wstHouseMesh.draw();
	}
	glPopMatrix();
}

void RendererWidget::drawSaloonMesh()
{
	glPushMatrix();
	{
		glTranslatef(150, 0, -100);
		glScalef(100, 100, 100);
		glRotatef(180, 0, 1, 0);
		saloonMesh.draw();
	}
	glPopMatrix();
}

void RendererWidget::drawBlacksmMesh()
{
	glPushMatrix();
	{
		glTranslatef(210, 0, -100);
		glScalef(100, 100, 100);
		glRotatef(-90, 0, 1, 0);
		blacksmMesh.draw();
	}
	glPopMatrix();
}

void RendererWidget::drawCarriageMesh()
{
	glPushMatrix();
	{
		glTranslatef(60, 0, -70);
		glRotatef(-20, 0, 1, 0);
		carriageMesh.draw();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(210, 0, -70);
		glRotatef(-40, 0, 1, 0);
		carriageMesh.draw();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-40, 0, -70);
		glRotatef(-90, 0, 1, 0);
		carriageMesh.draw();
	}
	glPopMatrix();
}

void RendererWidget::drawBarrelMesh()
{
	glPushMatrix();
	{
		glTranslatef(40, 0, -70);
		glScalef(0.01, 0.01, 0.01);
		glRotatef(-90, 0, 1, 0);
		barrelMesh.draw();
	}
	glPopMatrix();
}

void RendererWidget::drawStackedCrateMesh()
{
	glPushMatrix();
	{
		glPushMatrix();
		glTranslatef(0, 10, 0);
		glScalef(0.05, 0.05, 0.05);
		crate.draw();
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0, 5, 0);
		glScalef(0.05, 0.05, 0.05);
		crate.draw();
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0, 0, 0);
		glScalef(0.05, 0.05, 0.05);
		crate.draw();
		glPopMatrix();

		glPushMatrix();
		glTranslatef(6, 5, 0);
		glRotatef(-30, 0, 1, 0);
		glScalef(0.05, 0.05, 0.05);
		crate.draw();
		glPopMatrix();

		glPushMatrix();
		glTranslatef(6, 0, 0);
		glRotatef(-30, 0, 1, 0);
		glScalef(0.05, 0.05, 0.05);
		crate.draw();
		glPopMatrix();

		glPushMatrix();
		glTranslatef(-6, 0, 0);
		glRotatef(30, 0, 1, 0);
		glScalef(0.05, 0.05, 0.05);
		crate.draw();
		glPopMatrix();
	}
	glPopMatrix();
}

void RendererWidget::drawCrateMesh()
{
	glPushMatrix();
	{
		glTranslatef(-200, 0, -80);
		glScalef(0.05, 0.05, 0.05);
		crate.draw();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(0, 0, -70);
		glScalef(0.05, 0.05, 0.05);
		crate.draw();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-20, 0, -70);
		this->drawStackedCrateMesh();
	}
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-110, 0, -70);
	this->drawStackedCrateMesh();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(190, 0, -70);
	this->drawStackedCrateMesh();
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(100, 0, -70);
		glScalef(0.05, 0.05, 0.05);
		crate.draw();
	}
	glPopMatrix();
}

void RendererWidget::drawCowboys()
{

	glPushMatrix();
	{
		glTranslatef(-75, 7.2, 0); 
		glRotatef(-90, 1, 0, 0);
		// Going to the right
		glRotatef(90, 0, 0, 1);
		cowboyLeft->draw();
	}
	glPopMatrix();


	glPushMatrix();
	{
		glTranslatef(75, 7.2, 0); 
		glRotatef(-90, 1, 0, 0);
		// Going to the left  
		glRotatef(-90, 0, 0, 1);
		cowboyRight->draw();
	}
	glPopMatrix();

}

void RendererWidget::drawCactuses()
{
	for(int i = 0; i < 30; i++)
	{
		glPushMatrix();
		glTranslatef(cactusInit[i][0], 0, cactusInit[i][1]);
		Mesh::cactus(cactusInit[i][2], cactusInit[i][3], cactusInit[i][4], cactusInit[i][5], cactusInit[i][6], bool(int(cactusInit[i][7]) % 2), bool(int(cactusInit[i][8])%2));				
		glPopMatrix();
	}
}

void RendererWidget::drawTumbleweed()
{
	tumbleSpeed+= 0.1;

	if(tumbleSpeed > 100)
	{
		tumbleSpeed = -100;
	}

	if(windSpeed > 0 && shouldDrawTumbleweed)
	{
		for(int i = 0; i < 5; i++)
		{
			glPushMatrix();

			Materials::whiteShiny.set();
			glScalef(5, 5, 5);

			// translate via time
			glTranslatef(tumbleweedInit[i] + tumbleSpeed * windSpeed / 2.0, 1, -i * 10);
			// rotate via angle given by time
			glRotatef(tumbleSpeed * 10, 1, 1, 1);

			tumbleweedTex->load();
			glColor4f(1,1,1,0);
			Mesh::wanted();//(5, 5, 5);

			{
				glPushMatrix();
				glRotatef(90, 1, 0, 0);
				Mesh::wanted();
				glPopMatrix();
			}

			tumbleweedTex->unload();
			Materials::whiteShiny.unset();
			glColor4f(1,1,1,1);

			glPopMatrix();
		}
	}
}

// called every time the widget needs painting
void RendererWidget::paintGL()
{ 
	// clear the widget
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_DEPTH_TEST); // comment out depth test to observe the result
	glMatrixMode(GL_MODELVIEW);

	{

		glPushMatrix();

		drawSkysphere();
		drawBird();
		drawCactus();

		//Mesh::pallet(6, 8, 0.5, 0.5);
		drawLight();	
		drawTerrain();
		drawRocks();
		drawBulls();
		drawHorses();
		drawGallows();
		drawCemetery();
		drawWantedPoster();
		drawMap();
		drawSaloonMesh3();
		drawSaloonMesh2();
		drawVentMesh();
		drawWstHouseMesh();
		drawSaloonMesh();
		drawBlacksmMesh();
		drawCarriageMesh();
		drawBarrelMesh();
		drawCrateMesh();
		drawCowboys();

		drawCactuses();
		drawTumbleweed();
		drawDustParticles();

		glPopMatrix();
		
		
		
		glPushMatrix();
		glRotatef(180, 0, 1, 0);
		drawCactus();
		drawBulls();
		drawWantedPoster();
		drawSaloonMesh3();
		drawSaloonMesh2();
		drawVentMesh();
		drawWstHouseMesh();
		drawSaloonMesh();
		drawBlacksmMesh();
		drawCarriageMesh();
		drawBarrelMesh();
		drawCrateMesh();

		drawCactuses();

		glPopMatrix();


		{

			/*
			   glPushMatrix();
			   glTranslatef(30, 0, -300);
			   glScalef(30, 30, 30);

			   cliffTex->load();
			   Mesh::sphere();	
			   cliffTex->unload();

			   glPopMatrix();
			   */
		}



	}

	camera.load();
	glFlush();	
}
