#include <QApplication>
#include <QVBoxLayout>
#include "RendererWindow.h"

int main(int argc, char *argv[])
{
	// create the application
	QApplication app(argc, argv);
	glutInit(&argc, argv);

	// create a master widget
	RendererWindow *window = new RendererWindow(NULL);

	// resize the window
	window->resize(1024 + 350, 768 + 112);

	// show the label
	window->show();

	// start it running
	app.exec();

	// clean up
	//	delete controller;
	delete window;

	// return to caller
	return 0; 
} 
