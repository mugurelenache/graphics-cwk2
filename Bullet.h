#pragma once

class Bullet
{
	public:
		Bullet(float x, float y, float z);
		~Bullet();

		void draw();
		void updateBulletMotion();

	public:
		float speed = 25;
		float direction[3] = { 0 };
		float keyframes = 0;
};
